import string

'''Ce programmme permet de calculer de nombre de mots contunu dans ce livre et
le nombre de fois que chaque mot est utilise'''

mot_nbre = {}
fichier = open ("examen.txt","r")
fich = fichier.read()
fich = fich.lower()
fich = fich.replace(" ",".") ##Remplacement des espaces par des signes de ponctuation

for lignes in fich:
    if lignes in string.punctuation or lignes in string.whitespace:
       fich = fich.replace(lignes," ")

qte_mot = len(fich)

fich = fich.split()

##Comptage de le repetition de chaque mot
for word in fich:
    if word not in mot_nbre:
        mot_nbre[word] = 1
    else:
            mot_nbre[word] += 1

print mot_nbre##Affichage sous forme de dictionnaire le nombre de fois dont chaque mot est utilise
print "Le nombre de mots differents est",len(mot_nbre)
print "Le nombre de mot du texte est {} mots".format(qte_mot)##Affichage de la quantite de mot utilise dans le livre
fichier.close()
