import string
'''Ce programmme permet de mettre chaque mot du texte sur une ligne
ensuite d'enlever tous les espaces et toutes les signes de ponctuation, puis
toutes les lettres sont mises en minuscule'''

fichier = open ("motor_stories.txt","r") ##Ouverture du fichier
fich = fichier.read() ##Lecture du fichier
fich = fich.lower() ##Mise en lettre minuscule
fich = fich.strip() ##Suppression des espaces

##Suppression des signes de ponctuation
for lignes in fich:
    if lignes in string.punctuation:
       fich = fich.replace(lignes," ")

fich = fich.split()##Conversion du fichier en liste 

##Affichage des elements de la liste
for element in fich:
    print (element)
fichier.close()
