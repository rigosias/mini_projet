import random

def histogram(mot):

	d={}

	for lettre in mot:

		d[lettre]=d.get(lettre,0)+1

	return d

def choose_from_hist(mot):

	a = random.choice(histogram(mot).keys())
	return a , histogram(mot)[a],"/",len(mot)

print choose_from_hist (['a','a','a','a','b','b','c','c','d'])
